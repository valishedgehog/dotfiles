-- Indicate first time installation
local is_boostrap = false

require('options').setup()
require('custom').setup()

-- packer.nvim configuration
local conf = {
    profile = {
        enable = true,
        threshold = 0, -- the amount in ms that a plugins load time must be over for it to be included in the profile
    },

    display = {
        open_fn = function()
            return require('packer.util').float { border = 'rounded' }
        end,
    },
}

-- Check if packer.nvim is installed
-- Run PackerCompile if there are changes in this file
local function packer_init()
    local fn = vim.fn
    local install_path = fn.stdpath 'data' .. '/site/pack/packer/start/packer.nvim'
    if fn.empty(fn.glob(install_path)) > 0 then
        fn.system {
            'git',
            'clone',
            '--depth',
            '1',
            'https://github.com/wbthomason/packer.nvim',
            install_path,
        }
        is_boostrap = true
        vim.cmd [[packadd packer.nvim]]
    end

    -- Run PackerCompile if there are changes in this file
    local packer_grp = vim.api.nvim_create_augroup('packer_user_config', { clear = true })
    vim.api.nvim_create_autocmd(
        { 'BufWritePost' },
        { pattern = vim.fn.expand '$MYVIMRC', command = 'source <afile> | PackerCompile', group = packer_grp }
    )
end


-- Plugins
local plug_config = require('plugins');
local function plugins(use)
    use { 'wbthomason/packer.nvim' }
    use { 'lewis6991/impatient.nvim' }

    -- Better notifications
    use { 
        'rcarriga/nvim-notify',
        config = plug_config.notify,
    }

    -- Better icons
    use {
        'nvim-tree/nvim-web-devicons',
        module = 'nvim-web-devicons',
        config = function()
            require('nvim-web-devicons').setup { default = true }
        end,
    }

    -- Ascii graphics collection (for startup screen)
    use { 
        'MaximilianLloyd/ascii.nvim', 
        requires = { 'MunifTanjim/nui.nvim' }, 
    }

    -- Startup screen
    use {
        'goolord/alpha-nvim',
        requires = { 'nvim-tree/nvim-web-devicons' },
        config = plug_config.alpha,
    }

    -- Screensaver for alpha startup screen
    use {
        'folke/drop.nvim',
        event = 'VimEnter',
        config = plug_config.drop,
    }

    -- Bootstrap Neovim
    if is_boostrap then
        print 'Neovim restart is required after installation!'
        require('packer').sync()
    end

    -- Status line
    use {
        'nvim-lualine/lualine.nvim',
        event = 'BufReadPre',
        requires = { 'nvim-tree/nvim-web-devicons', opt = true},
        config = plug_config.lualine
    }

    -- Auto pairs
    use {
        'windwp/nvim-autopairs',
        opt = true,
        event = 'InsertEnter',
        module = { 'nvim-autopairs.completion.cmp', 'nvim-autopairs' },
        config = plug_config.autopairs,
    }

    -- Neovim Tree
    use {
        'nvim-tree/nvim-tree.lua',
        opt = true,
        cmd = { 'NvimTreeToggle', 'NvimTreeClose' },
        config = plug_config.nvimtree,
    }

    -- Telescope fuzzy finder
    use {
        'nvim-telescope/telescope.nvim',
        requires = { 'nvim-lua/plenary.nvim' },
        config = plug_config.telescope,
    }
    use {
        'nvim-telescope/telescope-file-browser.nvim',
        requires = { 'nvim-telescope/telescope.nvim', 'nvim-lua/plenary.nvim' }
    }
    use {
        'cljoly/telescope-repo.nvim',
        requires = { 'nvim-telescope/telescope.nvim', 'nvim-lua/plenary.nvim' }
    }

    -- Treesitter parser
    use {
        'nvim-treesitter/nvim-treesitter',
        run = function()
            local ts_update = require('nvim-treesitter.install').update({ with_sync = true })
            ts_update()
        end,
        config = plug_config.treesitter,
    }

    -- WhichKey
    use {
        'folke/which-key.nvim',
        config = function() 
            require('whichkey').setup()
        end
    }

    -- Git plugins
    use { 
        'f-person/git-blame.nvim',
        config = function()
            require('gitblame').setup { enabled = false }
        end
    }
    use {
        "NeogitOrg/neogit",
        requires = {
            "nvim-lua/plenary.nvim",
            "nvim-telescope/telescope.nvim",
            "sindrets/diffview.nvim",
            "ibhagwan/fzf-lua",
        },
        config = function()
            require('neogit').setup()
        end
      }
    use {
        "kdheepak/lazygit.nvim",
        requires = { "nvim-lua/plenary.nvim" },
    }
    use { 
        'lewis6991/gitsigns.nvim',
        config = function()
            require('gitsigns').setup()
        end
    }

    -- Other
    use {'stevearc/dressing.nvim'}
    use {
        "RRethy/nvim-treesitter-endwise",
        opt = true, 
        event = "InsertEnter",
    }

    -- Colorscheme
    use { 
        "rebelot/kanagawa.nvim",
        config = plug_config.kanagawa
    }

    -- Tab line / Buffer line
    use {
        "akinsho/bufferline.nvim",
        requires = { "nvim-tree/nvim-web-devicons" },
        config = function()
            require("bufferline").setup {
                options = {
                    numbers = "ordinal"
                }
            }
        end
    }

    -- Trouble pannel
    use {
        "folke/trouble.nvim",
        config = function()
            require("trouble").setup()
        end
    }

    -- Mason dependencies manager
    use {
        "williamboman/mason.nvim",
        config = function()
            require("mason").setup()
        end
    }
    use { "williamboman/mason-lspconfig.nvim" }

    -- LSP
    use { "hrsh7th/cmp-nvim-lsp" }
    use { "hrsh7th/cmp-nvim-lsp-signature-help" }
    use { "hrsh7th/cmp-buffer" }
    use { "hrsh7th/cmp-path" }
    use { "hrsh7th/cmp-cmdline" }
    use { "L3MON4D3/LuaSnip" }
    use { "saadparwaiz1/cmp_luasnip" }
    use { 
        "hrsh7th/nvim-cmp",
        config = function()
            require("lsp").cmp_setup()
        end
    }
    use { 
        "neovim/nvim-lspconfig",
        config = function() 
            require("lsp").setup()
        end
    }
end  

-- Init and start packer
packer_init()
local packer = require 'packer'

-- Performance
pcall(require, 'impatient')

packer.init(conf)
packer.startup(plugins)
