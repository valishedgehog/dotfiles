local M = {}



function M.alpha() 
    local status_ok, alpha = pcall(require, 'alpha')
    if not status_ok then
        return
    end

    local dashboard = require 'alpha.themes.dashboard'

    dashboard.section.header.val = require('ascii').get_random_global()

    dashboard.section.buttons.val = {
        dashboard.button('e', '  New file', ':ene <BAR> startinsert <CR>'),
        dashboard.button('c', '  Configuration', ':e $MYVIMRC <CR>'),
        dashboard.button('q', '󰩈  Quit Neovim', ':qa<CR>'),
    }

    local function footer()
        -- Number of plugins
        local total_plugins = #vim.tbl_keys(packer_plugins)
        local datetime = os.date '%d.%m.%Y %H:%M'
        local plugins_text = '   '
            .. total_plugins
            .. ' plugins'
            .. '        v'
            .. vim.version().major
            .. '.'
            .. vim.version().minor
            .. '.'
            .. vim.version().patch
            .. '        '
            .. datetime

        -- Quote
        local fortune = require 'alpha.fortune'
        local quote = table.concat(fortune(), '\n')

        return plugins_text .. '\n' .. quote
    end

    dashboard.section.footer.val = footer()

    dashboard.section.footer.opts.hl = 'Constant'
    dashboard.section.header.opts.hl = 'Include'
    dashboard.section.buttons.opts.hl = 'Function'
    dashboard.section.buttons.opts.hl_shortcut = 'Type'
    dashboard.opts.opts.noautocmd = true

    alpha.setup(dashboard.opts)
end

function M.autopairs()
    local npairs = require 'nvim-autopairs'
    npairs.setup {
      -- TODO set to true when TreeSitter will be configured
      check_ts = false,
    }
    -- npairs.add_rules(require 'nvim-autopairs.rules.endwise-lua')
end

function M.drop()
    local drop = require('drop')
    local theme = ({ 'leaves', 'stars', 'spring', 'summer', 'snow' })[math.random(1, 5)]
    
    return drop.setup {
        theme = theme,
        screensaver = 1000 * 60, 
        interval = 100, 
        max = 100,
        filetypes = {},
    }
end

function M.lualine()
    local theme_name = 'auto'

    -- If ayu theme installed use it
    local status_ok, ayu = pcall(require, 'ayu')
    if status_ok then
        theme_name = 'ayu'
    end

    require('lualine').setup {
        options = {
            icons_enabled = true,
            theme = theme_name,
            component_separators = {},
            section_separators = {},
            disabled_filetypes = {
                statusline = {},
                winbar = {
                    'help',
                    'startify',
                    'dashboard',
                    'packer',
                    'NvimTree',
                    'alpha',
                },
            },
            always_divide_middle = true,
            globalstatus = true,
        },
    }
end

function M.nvimtree()
    require('nvim-tree').setup {
        disable_netrw = false,
        hijack_netrw = true,
        view = {
            number = true,
            relativenumber = true,
        },
        filters = {
            custom = { '.git' },
        },
        sync_root_with_cwd = true,
        respect_buf_cwd = true,
        update_focused_file = {
            enable = true,
            update_root = true,
        },
    }
end

function M.notify()
    local notify = require('notify')
    notify.setup {
        background_colour = '#000000',
        max_width = 80,
        max_height = 30,
    }
    vim.notify = notify
end

function M.telescope()
    require('telescope').setup {
        extensions = {
            'notify',
            'file_browser',
            'repo',
            'projects',
        },
    }
end

function M.treesitter()
    require("nvim-treesitter.configs").setup {
        incremental_selection = {
            enable = true,
        },
        highlight = {
            enable = true,
            additional_vim_regex_highlighting = false,
        },
    }

    ---WORKAROUND (for packer users, see https://github.com/nvim-treesitter/nvim-treesitter/wiki/Installation)
    -- vim.opt.foldmethod     = 'expr'
    -- vim.opt.foldexpr       = 'nvim_treesitter#foldexpr()'
    vim.api.nvim_create_autocmd({'BufEnter','BufAdd','BufNew','BufNewFile','BufWinEnter'}, {
        group = vim.api.nvim_create_augroup('TS_FOLD_WORKAROUND', {}),
        callback = function()
        vim.opt.foldmethod     = 'expr'
        vim.opt.foldexpr       = 'nvim_treesitter#foldexpr()'
        end
    })
    ---ENDWORKAROUND
end

function M.kanagawa() 
    require("kanagawa").setup {
        -- Remove gutter background
        colors = {
            theme = {
                all = {
                    ui = {
                        bg_gutter = "none"
                    }
                }
            }
        },
        overrides = function(colors)
            local theme = colors.theme
            return {
                -- Nice floating windows
                NormalFloat = { bg = "none" },
                FloatBorder = { bg = "none" },
                FloatTitle = { bg = "none" },
                NormalDark = { fg = theme.ui.fg_dim, bg = theme.ui.bg_m3 },
                LazyNormal = { bg = theme.ui.bg_m3, fg = theme.ui.fg_dim },
                MasonNormal = { bg = theme.ui.bg_m3, fg = theme.ui.fg_dim },
                
                -- Modern telescope look
                TelescopeTitle = { fg = theme.ui.special, bold = true },
                TelescopePromptNormal = { bg = theme.ui.bg_p1 },
                TelescopePromptBorder = { fg = theme.ui.bg_p1, bg = theme.ui.bg_p1 },
                TelescopeResultsNormal = { fg = theme.ui.fg_dim, bg = theme.ui.bg_m1 },
                TelescopeResultsBorder = { fg = theme.ui.bg_m1, bg = theme.ui.bg_m1 },
                TelescopePreviewNormal = { bg = theme.ui.bg_dim },
                TelescopePreviewBorder = { bg = theme.ui.bg_dim, fg = theme.ui.bg_dim },

                -- Nice popup menu
                Pmenu = { fg = theme.ui.shade0, bg = theme.ui.bg_p1 },
                PmenuSel = { fg = "NONE", bg = theme.ui.bg_p2 },
                PmenuSbar = { bg = theme.ui.bg_m1 },
                PmenuThumb = { bg = theme.ui.bg_p2 },
            }
        end,
    }

    vim.cmd("colorscheme kanagawa")
end

return M
