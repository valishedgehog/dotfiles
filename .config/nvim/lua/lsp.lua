local M = {}

function M.setup()
    require("mason").setup()
    require("mason-lspconfig").setup()

    local lspconfig = require("lspconfig")
    local lsp_defaults = lspconfig.util.default_config

    lsp_defaults.capabilities = vim.tbl_deep_extend(
        'force',
        lsp_defaults.capabilities,
        require('cmp_nvim_lsp').default_capabilities()
    )

    lspconfig.phpactor.setup {
        root_dir = function(_)
            return vim.loop.cwd()
        end,
        init_options = { 
            ["language_server.diagnostics_on_update"] = true,
            ["language_server.diagnostics_on_open"] = true,
            ["language_server.diagnostics_on_save"] = true,
            ["language_server_phpstan.enabled"] = true,
            ["language_server_psalm.enabled"] = false,
        }
    }
end

function M.cmp_setup()
    local cmp = require("cmp");
    local luasnip = require('luasnip')
    require('luasnip.loaders.from_vscode').lazy_load()
    local select_opts = {behavior = cmp.SelectBehavior.Select}

    cmp.setup({
        snippet = {
            expand = function(args)
                luasnip.lsp_expand(args.body)
            end,
        },
        sources = {
            {name = 'path'},
            {name = 'nvim_lsp', keyword_length = 3},
            {name = 'buffer', keyword_length = 3},
            {name = 'luasnip', keyword_length = 2},
        },
        window = {
            documentation = cmp.config.window.bordered()
        },
        formatting = {
            fields = {'menu', 'abbr', 'kind'},
            format = function(entry, item)
                local menu_icon = {
                  nvim_lsp = 'λ',
                  luasnip = '⋗',
                  buffer = 'Ω',
                  path = '🖫',
                }
            
                item.menu = menu_icon[entry.source.name]
                return item
            end,
        },
        mapping = {
            ['<Up>'] = cmp.mapping.select_prev_item(select_opts),
            ['<Down>'] = cmp.mapping.select_next_item(select_opts),
            ['<C-p>'] = cmp.mapping.select_prev_item(select_opts),
            ['<C-n>'] = cmp.mapping.select_next_item(select_opts),
            ['<C-u>'] = cmp.mapping.scroll_docs(-4),
            ['<C-f>'] = cmp.mapping.scroll_docs(4),
            ['<C-e>'] = cmp.mapping.abort(),
            ['<CR>'] = cmp.mapping.confirm({select = false}),
            ['<Tab>'] = cmp.mapping(function(fallback)
              local col = vim.fn.col('.') - 1
        
              if cmp.visible() then
                cmp.select_next_item(select_opts)
              elseif col == 0 or vim.fn.getline('.'):sub(col, col):match('%s') then
                fallback()
              else
                cmp.complete()
              end
            end, {'i', 's'}),
            ['<S-Tab>'] = cmp.mapping(function(fallback)
              if cmp.visible() then
                cmp.select_prev_item(select_opts)
              else
                fallback()
              end
            end, {'i', 's'}),
          },
    })
end

return M