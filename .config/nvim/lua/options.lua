local M = {}

function M.setup()
    vim.opt.termguicolors = true
    vim.opt.timeoutlen = 300
    vim.opt.number = true
    vim.opt.relativenumber = true
    vim.opt.mouse = nvi
    vim.opt.completeopt = {'menu', 'menuone', 'noselect'}
    vim.opt.tabstop = 4
    vim.opt.shiftwidth = 4
end

return M