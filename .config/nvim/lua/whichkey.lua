local M = {}

local function normal_keymap()
    local _opts = {
        mode = 'n',
        prefix = '<leader>',
        buffer = nil,
        silent = true,
        noremap = true,
        nowait = false,
    }

    local _keymap = {
        g = {
            name = "Git tools",
            l = { "<cmd>LazyGit<cr>", "LazyGit window" },
            n = { "<cmd>Neogit<cr>", "Neogit" },
            b = { "<cmd>GitBlameToggle<cr>", "Git blame toggle" },
            t = { 
                name = "Telescope",
                b = { "<cmd>lua require('telescope.builtin').git_branches()<cr>", "Git branches" },
                c = { "<cmd>lua require('telescope.builtin').git_commits()<cr>", "Git commits" },
                f = { "<cmd>lua require('telescope.builtin').git_files()<cr>", "Git files" },
                s = { "<cmd>lua require('telescope.builtin').git_status()<cr>", "Git status" },
                S = { "<cmd>lua require('telescope.builtin').git_stash()<cr>", "Git stash" }
            },
            s = {
                name = "Gitsigns",
                R = { "<cmd>Gitsigns reset_buffer<cr>", "Reset buffer" },
                r = { "<cmd>Gitsigns reset_hunk<cr>", "Reset hunk" },
                S = { "<cmd>Gitsigns stage_buffer<cr>", "Stage buffer" },
                s = { "<cmd>Gitsigns stage_hunk<cr>", "Stage hunk" },
                n = { "<cmd>Gitsigns next_hunk<cr>", "Next hunk" },
                p = { "<cmd>Gitsigns previous_hunk<cr>", "Previous hunk" },
                d = { "<cmd>Gitsigns diff_this<cr>", "Diff this" },
            }
        },

        b = {
            name = "Buffer line",
            n = { "<cmd>BufferLineCycleNext<cr>", "Next buffer" },
            p = { "<cmd>BufferLineCyclePrev<cr>", "Previous buffer" },
            P = { "<cmd>BufferLineTogglePin<cr>", "Buffer toggle pin" },
            s = { "<cmd>BufferLinePick<cr>", "Buffer line pick" },
            g = { ":BufferLineGoToBuffer ", "Go to buffer" },
            S = { 
                name = "Sort buffers",
                d = { "<cmd>BufferLineSortByDirectory<cr>", "Sort by directory" },
                e = { "<cmd>BufferLineSortByExtension<cr>", "Sort by extension" },
                r = { "<cmd>BufferLineSortByRelativeDirectory<cr>", "Sort by relarive directory" },
                t = { "<cmd>BufferLineSortByTabs<cr>", "Soft by tabs" },
            },
        },

        o = {
            name = "Other",
            f = { "<cmd>NvimTreeToggle<cr>", "NvimTree Explorer" },
            m = { "<cmd>Mason<cr>", "Mason"}
        },

        t = {
            name = "Telescope",
            f = { "<cmd>lua require('telescope.builtin').find_files()<cr>", "Files" },
            b = { "<cmd>lua require('telescope.builtin').buffers()<cr>", "Buffers" },
            h = { "<cmd>lua require('telescope.builtin').help_tags()<cr>", "Help" },
            m = { "<cmd>lua require('telescope.builtin').marks()<cr>", "Marks" },
            o = { "<cmd>lua require('telescope.builtin').oldfiles()<cr>", "Old Files" },
            g = { "<cmd>lua require('telescope.builtin').live_grep()<cr>", "Live Grep" },
            c = { "<cmd>lua require('telescope.builtin').commands()<cr>", "Commands" },
            w = { "<cmd>lua require('telescope.builtin').current_buffer_fuzzy_find()<cr>", "Current Buffer" },
            e = { "<cmd>lua require('telescope').extensions.file_browser.file_browser()<cr>", "File Browser" },
            n = { "<cmd>lua require('telescope').extensions.notify.notify()<cr>", "Notifications" },
            r = { "<cmd>lua require('telescope').extensions.repo.repo()<cr>", "Repositories" },
            p = { "<cmd>:Telescope neovim-project discover<cr>", "Projects" },
        },
    }

    return {
        opts = _opts,
        keymap = _keymap,
    }
end

local function visual_keymap()
    local _opts = {
        mode = 'v',
        prefix = '<leader>',
        buffer = nil,
        silent = true,
        noremap = true,
        nowait = false,
    }

    return {
        opts = _opts,
        keymap = {},
    }
end

function M.setup()
    local whichkey = require('which-key')
    whichkey.setup {
        window = {
            border = 'single',
            position = 'bottom',
        },
    }

    local n_keymap = normal_keymap()
    local v_keymap = visual_keymap()
    whichkey.register(n_keymap.keymap, n_keymap.opts)
    whichkey.register(v_keymap.keymap, v_keymap.opts)
end

return M